This is a 2d integral brute-force approximation calculator for the command line.
If you are a little braindead and don't know the basics of integral calculus, see: 
 INTEGRAL BASICS
If you are interested in how exactly this program approximates integrals, see: 
 OPERATIONAL PRINCIPLES

--PROGRAM USAGE--

Each relevant input (slope, low boundry, high boundry and sample rate of slope function)
 are entered as arguments.

The arguments entered are exactly 4 and require a specific order, which is shown as follows:

             Upper
            Boundry
               |
     Slope     |
    Function   |
       |       |
integ 5*x3/ 0 100 100
            |     |
          Lower   |
         Boundry  |
                  |
                Sample
                 Rate

The output is the integral of the slope function on a single line which, in this case, outputs
 the following: 

8333.33

The slope function is in a sort of loose reverse polish or stack notation. Here are the operations.
 
 +: Addition.
 -: Subtraction.
 *: Multiplication.
 /: Division.
 ^: Exponents (converts exponent to positive integer).
 ,: Separate decimal values from one another (you can also use spaces).
 !: Swaps values on the stack so the left becomes the second operand and the right becomes the first.

The notation of this function is incredibly free. Variables/values are parsed into a separate stack to
 operations so the following three slope functions are equivalent:

 5+2x*

 5,2x+*

 +*5,2x

"5 2x+*"

"5 + 2 x *"		( Remember that the function fits in a single argument and so it must be
                           enclosed in quote when it contains spaces. )

 5+2*x			( While this is valid, it gives the impression that it is under normal notation
 			   rules, which is a problem when using the program in scripts. )

The benefit here is being able to place all the operations in a function together, next to all the
 values/variables. It can be much easier to see, at a glance, all of the operations to be done and 
 should be done if such a function is simple enough to warrant the disruption of intimacy between each 
 operation and its operands.

For those unfamiliar with reverse Polish notation, the previous function is equivalent to y=2x+5.

Value swapping is a sort of necessary feature resulting from the rigid nature of stack-based computation.
Here is an example of swapping and it's normal notation counterpart.

 2!/x4+
 (x+4)/2

Many operations in math are non-commutable so being able to swap operator and operand vastly improves
 composability of functions.

--INTEGRAL BASICS--

The basics of integrals amounts to finding the geometric area that a curve or slope creates.

8|       /
7|      /X
6|     /XX
5|    /XXX
4|   /XXXX
3|  /XXXXX
2| /XXXXXX
1|/XXXXXXX
 +--------
  12345678

In the above example graph, which represents a slope of y=x, the filled in section represents the integral
 of that function.

Because values for x are theoretically infinite, this integral is only an abstraction of a theoretical
 area. To find a definite area that one can use as values in other calculations, one must limit this
 integration to only a range of values for x. The ends of this range are called the upper and lower
 bounds.

For a simple slope function like y=x, finding this is a simple matter of solving for the area of an
 isoceles right triangle from one of the equal sides, or half a square. For bounds of 0 to 8, this
 yields a result of 32 or (8*8)/2. Many integrals can be solved this way, there being no shortage of
 algebra tricks to make simple their calculation. However these take time and mental energy, which is why
 I created this program to approximate them with the desired accuracy.

--OPERATIONAL PRINCIPLES--

This program approximates integrals using triangle-topped, slice-area approximation.

This program operates via stack-based mathematics, it being an RPN calculator, so it literally has a 
 stack-based memory model. This means that the last values in are the first to be calculated. To allow
 freer function syntax (and to simplify the memory code) this model is split into effectively two stacks: 
 the value/variable stack and the operation stack. 

This program uses a function pointer array for slope operation execution for multiple values of X.
