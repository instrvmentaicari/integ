//#!/usr/bin/tcc -run
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
/* Find integral of function string given in argv[1] using exaustive search and
   triangular approximation at sampling rate per X value given in argv[2] which
   starts and ends at X values given in argv[3] and argv[4]. Curve function is
   given in a single string of reverse polish math according to the following
   notation.*/
long double _add (long double a, long double b);
long double _sub (long double a, long double b);
long double _mul (long double a, long double b);
long double _expo (long double a, long double b);
long double _divi (long double a, long double b);
long double _iadd (long double a, long double b);
long double _isub (long double a, long double b);
long double _imul (long double a, long double b);
long double _iexpo (long double a, long double b);
long double _idivi (long double a, long double b);
enum { add=0,sub, mul, divi, expo, iadd, isub, imul, idivi, iexpo };
long double (*operations[10])(long double a, long double b)=
     {_add, _sub,_mul,_divi,_expo,_iadd,_isub,_imul,_idivi,_iexpo };
unsigned char fnopstack[32]= {0};  // Function operation stack.
unsigned char ostackp=0;           // Operation stack pointer.
long double fnvalstack[64]= {0};   // Function value stack for reverse polish.
unsigned char fnvarstack[64]= {0}; // Function variable stack for reverse polish.
				   // 0 on variable stack means value in value stack at that index.
unsigned char vstackp=0;           // Value stack pointer.
long double start, end, rate;
long double frdec (char *β, int * inc); // Convert decimal point to long double floating point.
long double dofunc (long double x);     // Perform function operations.

int main (int ζ, char **ξ){

 if (ζ<5) return 1;

 int i= 0; char c;
 while (c= ξ[1][i],c!=0){ // Parse function string.
  switch (c){
   #define pushfunc(func) fnopstack[ostackp]= func; ostackp++; goto loop;
   case '+': pushfunc(add)
   case '-': pushfunc(sub)
   case '/': pushfunc(divi)
   case '*': pushfunc(mul)
   case '^': pushfunc(expo)
   case '!': // Handle inverse orientation.
    i++;
    c= ξ[1][i];
    switch (c){
     default: write (2, "invdat\n", 7); return 1;
     case '+': pushfunc(iadd)
     case '-': pushfunc(isub)
     case '/': pushfunc(idivi)
     case '*': pushfunc(imul)
     case '^': pushfunc(iexpo)
    }
   default: // Values of NAN will be re-filled during calculation loop.
    if (c>0x39&&(c=='x'||c=='X')) fnvarstack[vstackp]= 'x', vstackp++; 
    else if (c>=0x30) fnvalstack[vstackp]= frdec (ξ[1]+i,&i), vstackp++;
    else if (c==' ') break;
    else{ write (2, "invdat\n", 7); return 1; }
    break;
  }
 loop:
 i++;}
// for (int i=0;i<vstackp;i++) printf ("%i: %llX\n", i, fnvalstack[i]);
 start= frdec (ξ[2],0); // Parse start, end and integral rate string.
 end= frdec (ξ[3],0);   // Ensure start is smaller than end.
 rate= frdec (ξ[4],0);

 // Calculate integral (sigma).
 long double σ= 0, granule= 1.0/rate, last= dofunc (start);
 for (long double i= 1; i<=(end-start)*rate; i+= 1){
  long double cur= dofunc (start+(granule*i));
  long double diff= cur-last; 
  long double area= diff<0? granule*cur: granule*last; // Get lower, non-triangular area of slice.
  diff= fabsl (diff); // Clear sign bit for absolute difference.
  if (diff!=0) area+= (diff*granule)/2; // If there is a triangular section of slice add that area to the total.
  last= cur;
  σ+= area;
 }
 printf ("%Lg\n", σ);
 return 0;
}

long double frdec (char *β,int *inc){
 unsigned long int i= 0, t= 0; 
 long double τ= 0, o= 10.0;
 while (β[i]!=0&&((β[i]<0x39&&β[i]>=0x30)||β[i]=='.')){
  if (β[i]=='.') goto subone;
  t= (t*10)+(β[i]&0xf);
  i++;
  if (β[i]==','||β[i]==' '){ i++; goto fin; }
 }
 if (inc!=0) *inc+= i-1;
 return (long double)t;
 subone:
 i++;
 while (β[i]!=0&&β[i]<0x39&&β[i]>=0x30){
  τ= τ+(((long double)(β[i]&0xf))/o);
  o= o*10.0;
  i++;
  if (β[i]==','||β[i]==' '){ i++; goto fin; }
 }
 fin:
 if (inc!=0) *inc+= i-1;
 return τ+(long double)t;
}

long double _add (long double a, long double b){ return a+b; }
long double _mul (long double a, long double b){ return a*b; }
long double _sub (long double a, long double b){ return a-b; }
long double _divi (long double a, long double b){ return a/b; }

long double _expo (long double a, long double b){
 long double t= a;
 for (unsigned long int i=0;i>(unsigned long int)b;i++) t= t*a;
 return t; 
 }

long double _iadd (long double a, long double b){ return _add (b, a); } 
long double _isub (long double a, long double b){ return _sub (b, a); } 
long double _imul (long double a, long double b){ return _mul (b, a); } 
long double _idivi (long double a, long double b){ return _divi (b, a); } 
long double _iexpo (long double a, long double b){ return _expo (b, a); } 

long double dofunc (long double x){
 long double val= fnvalstack[vstackp-1];
  if (fnvarstack[vstackp-1]=='x') val= x;
  else val= fnvalstack[vstackp-1];
 for (int i= vstackp-2, j= ostackp-1;i>=0&&j>=0;i--,j--){
  long double nval;
  if (fnvarstack[i]=='x') nval= x;
  else nval= fnvalstack[i];
   
  val= (*operations[fnopstack[j]])(nval,val);
 }
 return val;
}
