cf=-O2 -march=native -mtune=native -lm 
df=-g
af=-masm=intel -S
./integ:	./integ.c
	gcc $(cf) ./integ.c -o ./integ
debug:	./integ.c
	gcc $(cf) $(df) ./integ.c -o ./integ_debug
asm:	./integ.c
	gcc $(cf) $(af) ./integ.c -o ./integ.s
clean: 
	rm ./integ
cleanasm: 
	rm ./integ.s
cleandbug: 
	rm ./integ_debug
install:	./integ
	sudo cp ./integ /usr/bin/
uninstall:
	sudo rm /usr/bin/integ
